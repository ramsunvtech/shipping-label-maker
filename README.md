# Shipping Label Maker

## Pros:
1. Reusable Wizard with Dynamic Steps is Implemented by passing Context.
2. Added Material UI for Progress bar, Input and Buttons.
3. Better Context suggested and added it as separate file in Util directory.
4. Screenshots are in the screenshots directory.
5. Added Few more props in steps would help and avoid unnecessary manipulation.

### Cons:
1. Shipping Cost Calculation is not completed due to release time in my current project.
2. No Time for Bonus points.

```
{
    id: 'from',
    title: "Enter the receiver's address",
    fieldList: [{
        name: 'name',
        label: 'Receiver Name',
        type: 'string'
    }, {
        name: 'street',
        label: 'Street',
        type: 'string'
    }, {
        name: 'city',
        label: 'City',
        type: 'string'
    }, {
        name: 'state',
        label: 'State',
        type: 'string'
    }, {
        name: 'zip',
        label: 'Zip',
        type: 'string'
    }]
}
```

