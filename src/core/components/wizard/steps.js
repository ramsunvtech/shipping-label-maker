import React, { Component } from 'react';

import InputBox from './input-box';
import Button from './button';

const totalProperties = (obj) => {
    if(!obj) {
        return 0;
    }

    return Object.getOwnPropertyNames(obj).length;
}

class Steps extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: props.id,
            fields: {}
        };
    }

    static getDerivedStateFromProps(nextProps, nextStates) {
        let fields = {};

        if (nextProps.id !== nextStates.id) {
            return {
                id: nextProps.id,
                fields: nextProps.fieldData || {}
            };
        } else if (nextProps.id === nextStates.id && nextStates.fields && totalProperties(nextStates.fields) > 0) {
            return {
                ...nextStates,
                fields: nextStates.fields
            }
        }

        return {
            ...nextStates,
            fields
        };
    }

    saveField = (name) => (value, isValid) => {
        const fields = this.state.fields;

        fields[name] = value;

        this.setState({
            fields,
        });
    }

    getFormFields() {
        const { id, wizardContext } = this.props;
        const fieldValues = this.state.fields;

        return (
            <React.Fragment>
                {wizardContext.map(field => {
                    if (field.type === 'string' || field.type === 'number') {
                        return (
                            <InputBox
                                key={`${id}-${field.name}`}
                                name={field.name}
                                label={field.label}
                                value={fieldValues[field.name] || ''}
                                onChange={this.saveField(field.name)}
                            />
                        );
                    }

                    return null;
                })}
            </React.Fragment>
        );
    }

    getPrevButtons() {
        const { showPrevious, onAction, id } = this.props;
        const { fields } = this.state;
       
        if (showPrevious) {
            return <Button label="Previous" onClick={() => onAction(-1, id, fields)} />;
        }

        return null;
    }

    getNextButtons() {
        const { showNext, onAction, id } = this.props;
        const { fields } = this.state;

        if (showNext) {
            return <Button label="Next" onClick={() => onAction(1, id, fields)} />;
        }

        return <Button label="Confirm" color="primary" onClick={() => onAction(1, id, fields, true)} />;
    }

    render() {
        return (
            <form>
                <fieldset className={`step-form ${this.state.id}`}>
                    <legend>{this.props.title}</legend>
                    {this.getFormFields()}
                    <div className="buttons">
                        {this.getPrevButtons()}
                        {this.getNextButtons()}
                    </div>
                </fieldset>
            </form>
        );
    }
}

export default Steps;

