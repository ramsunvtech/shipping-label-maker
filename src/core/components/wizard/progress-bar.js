import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = {
    root: {
      flexGrow: 1,
    },
};

function ProgressBar({ status }) {
    return (
        <React.Fragment>
            <LinearProgress variant="determinate" value={status} />
        </React.Fragment>
    );
}

ProgressBar.propTypes = {
    classes: PropTypes.object.isRequired,
    status: PropTypes.number.isRequired,
};
  
export default withStyles(styles)(ProgressBar);