import React, { Component } from 'react';

import ProgressBar from './progress-bar';
import Steps from './steps';
import Button from './button';

const totalItems = (arrayVar) => {
    if(!Array.isArray(arrayVar) || arrayVar.length === 0) {
        return 0;
    }

    return arrayVar.length;
}

class Wizard extends Component {
    constructor(props) {
        super(props);
        const totalSteps = totalItems(props.wizardContext);

        this.state = {
            data: {},
            action: {
                prev: 0,
                next: 2,
                end: totalSteps,
            },
            completion: 0,
        };
    }

    onComplete = () => () => {
        this.props.onComplete();
    }

    onAction = () => (go, id, fields, isComplete) => {
        const data = this.state.data;
        const { prev, next, end  } = this.state.action;
        const prevNew = (go === -1) ? (prev - 1) : (prev + 1);
        const nextNew = (go === 1) ? (next + 1) : (next - 1);
        const completion = Math.round((prevNew/end)*100);

        data[id] = fields;

        if (isComplete) {
            this.setState({
                data,
                completion: '100',
                action: {
                    prev: prevNew,
                    next: nextNew,
                    end
                }
            });
        }

        this.setState({
            data,
            completion,
            action: {
                prev: prevNew,
                next: nextNew,
                end
            }
        });
    }

    getSteps() {
        const { action: { prev, next, end } } = this.state;
        const { wizardContext } = this.props;
        const currentStep = prev + 1;
        const { id, title, fieldList = {} } = wizardContext[prev];
        const showPrevButton = (prev > 0);
        const showNextButton = (next <= end);
        const fieldData = this.state.data[id] || {};

        return (
            <React.Fragment>
                <Steps
                    id={id}
                    title={title}
                    index={currentStep}
                    showPrevious={showPrevButton}
                    showNext={showNextButton}
                    fieldData={fieldData}

                    wizardContext={fieldList}
                    onAction={this.onAction()}
                />
            </React.Fragment>
        );
    }

    getConfirm() {
        const formData = this.state.data;
        let stepList = [];

        stepList.push((
            <React.Fragment>
                <div className="complete">
                    <Button label="Complete" color="primary" onClick={() => this.props.onComplete()} />
                </div>
            </React.Fragment>
        ));

        for(let stepId in formData) {
            let fieldList = [];
            fieldList.push((
                <React.Fragment>
                    <h2 className="capitalize">{stepId}</h2>
                </React.Fragment>
            ));

            for(let field in formData[stepId]) {
                fieldList.push((
                    <React.Fragment>
                        <h4 className="capitalize">{field}: {formData[stepId][field]}</h4>
                    </React.Fragment>
                ));
            }

            stepList.push(<div>{fieldList}</div>);
        }

        return stepList;
    }

    getWizardBody() {
        if (this.state.completion === 100) {
            return (
                <React.Fragment>
                    {this.getConfirm()}
                </React.Fragment>
            );
        }

        return (
            <React.Fragment>
                {this.getSteps()}
            </React.Fragment>
        );
    }

    render() {
        return (
            <div className="steps-wizard">
                <ProgressBar status={this.state.completion} />
                {this.getWizardBody()}
            </div>
        );
    }
}

export default Wizard;

