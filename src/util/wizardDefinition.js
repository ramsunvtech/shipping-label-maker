export const wizardDefinition = [{
    id: 'from',
    title: "Enter the receiver's address",
    fieldList: [{
        name: 'name',
        label: 'Receiver Name',
        type: 'string'
    }, {
        name: 'street',
        label: 'Street',
        type: 'string'
    }, {
        name: 'city',
        label: 'City',
        type: 'string'
    }, {
        name: 'state',
        label: 'State',
        type: 'string'
    }, {
        name: 'zip',
        label: 'Zip',
        type: 'string'
    }]
}, {
    id: 'to',
    title: "Enter the sender's address",
    fieldList: [{
    name: 'name',
    label: 'Sender Name',
    type: 'string'
}, {
    name: 'street',
    label: 'Street',
    type: 'string'
}, {
    name: 'city',
    label: 'City',
    type: 'string'
}, {
    name: 'state',
    label: 'State',
    type: 'string'
}, {
    name: 'zip',
    label: 'Zip',
    type: 'string'
}]
}, {
    id: 'weight',
    title: "Shipping Weight",
    fieldList: [{
        name: 'weight',
        label: 'Weight',
        type: 'number'
    }]
}, {
    id: 'option',
    title: "Shipping Option",
    fieldList: [{
        name: 'ground',
        label: 'Ground',
        type: 'number'
    }, {
        name: 'priority',
        label: 'Priority',
        type: 'number'
    }]
}];