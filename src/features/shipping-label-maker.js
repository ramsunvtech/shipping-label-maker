import React from 'react';

import Wizard from '../core/components/wizard/wizard';
import { wizardDefinition } from '../util/wizardDefinition';

import '../css/shipping-label.css';

function ShippingLabelMaker() {
    function onComplete() {
        alert('Completed !');
    }

    return (
        <div className="shipping-label-maker">
            <h1>Shipping Label Maker</h1>
            <Wizard
                header={() => {}}
                steps={wizardDefinition}
                wizardContext={wizardDefinition}
                onComplete={onComplete}
            />
        </div>
    );
}

export default ShippingLabelMaker;